﻿using Harmony.Infrastructure.Common.ExtenstionMethods;
using NUnit.Framework;
using System;

namespace Harmony.Infrastructure.Common.UnitTests.ExtenstionMethods
{
	[TestFixture]
	public class ObjectMethodsTests
	{
		[TestCase(null, true, TestName = "IsNull_ValueIsNull")]
		[TestCase(1, false, TestName = "IsNull_ValueIsNotNull")]
		public void IsNullCases(object value, bool expectedResult)
		{
			#region Act

			var actualResult = value.IsNull();

			#endregion

			#region Assert

			Assert.That(actualResult, Is.EqualTo(expectedResult));

			#endregion
		}

		[TestCase(null, false, TestName = "IsNull_ValueIsNull")]
		[TestCase(1, true, TestName = "IsNull_ValueIsNotNull")]
		public void HasValueCases(object value, bool expectedResult)
		{
			#region Act

			var actualResult = value.HasValue();

			#endregion

			#region Assert

			Assert.That(actualResult, Is.EqualTo(expectedResult));

			#endregion
		}

		[TestCase(null, 2, 2, TestName = "IsEmptySetValue_ValueIsNull")]
		[TestCase(1, 3, 1, TestName = "IsEmptySetValue_ValueIsNotNull")]
		public void IsNullSetValueCases(object value, object newValue, object expectedResult)
		{
			#region Act

			var actualResult = value.IsNull(newValue);

			#endregion

			#region Assert

			Assert.That(actualResult, Is.EqualTo(expectedResult));

			#endregion
		}

		[TestCase(null, "TestErrorMessage1", typeof(Exception),
			TestName = "ThrowIfIsNullSetException_ValueIsEmptyStringWaitException")]
		[TestCase("TestValue", null, null, TestName = "ThrowIfIsNullSetException_WithoutException")]
		[TestCase(null, "TestErrorMessage2", typeof(NullReferenceException),
			TestName = "ThrowIfIsNullSetException_WaitNullReferenceException")]
		public void ThrowIfIsNullSetExceptionCases(
			object value, string errorMessage, Type expectedExceptionType)
		{
			#region Arrange

			Exception expectedException = null;
			if (expectedExceptionType != null)
				expectedException = (Exception)Activator.CreateInstance(expectedExceptionType, errorMessage);

			void ActionForTest()
			{
				value.ThrowIfIsNull(expectedException);
			}

			#endregion

			#region Assert

			if (expectedExceptionType == null)
				Assert.DoesNotThrow(ActionForTest);
			else
			{
				var actualExсeption = Assert.Throws(Is.TypeOf(expectedExceptionType), ActionForTest);
				Assert.That(actualExсeption.Message, Is.EqualTo(errorMessage));
			}

			#endregion
		}

		[TestCase("TestValue", false, TestName = "ThrowIfArgumentIsNull_ValueIsSampleString")]
		[TestCase(null, true, TestName = "ThrowIfArgumentIsNull_ValueIsNull")]
		public void ThrowIfArgumentIsNullCases(object value, bool exceptionIsExpected)
		{
			#region Arrange

			const string argumentName = "Argument1";

			void ActionForTest()
			{
				value.ThrowIfArgumentIsNull(argumentName);
			}

			#endregion

			#region Assert

			if (exceptionIsExpected)
			{
				var actualException = Assert.Throws<ArgumentNullException>(ActionForTest);

				StringAssert.Contains(argumentName, actualException.Message);
			}
			else
				Assert.DoesNotThrow(ActionForTest);

			#endregion
		}

		[TestCase(null, "TestErrorMessage1", true,
			TestName = "ThrowIfIsNullSetMessage_ValueIsEmptyStringWaitException")]
		[TestCase("TestValue", "TestErrorMessage2", false, TestName = "ThrowIfIsNullSetMessage_WithoutException")]
		public void ThrowIfIsNullSetMessageCases(
			object value, string errorMessage, bool expectException)
		{
			#region Arrange

			void ActionForTest()
			{
				value.ThrowIfIsNull(errorMessage);
			}

			#endregion

			#region Assert

			if (!expectException)
				Assert.DoesNotThrow(ActionForTest);
			else
			{
				var actualException = Assert.Throws<NullReferenceException>(ActionForTest);
				Assert.That(actualException.Message, Is.EqualTo(errorMessage));
			}

			#endregion
		}
	}
}