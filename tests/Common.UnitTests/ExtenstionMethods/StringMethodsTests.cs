﻿using Harmony.Infrastructure.Common.ExtenstionMethods;
using NUnit.Framework;
using System;

namespace Harmony.Infrastructure.Common.UnitTests.ExtenstionMethods
{
	[TestFixture]
	public class StringMethodsTests
	{
		[TestCase("", true, TestName = "IsEmpty_ValueIsEmptyString")]
		[TestCase("TestValue", false, TestName = "IsEmpty_ValueIsSampleString")]
		[TestCase(null, true, TestName = "IsEmpty_ValueIsNull")]
		[TestCase("\n", true, TestName = "IsEmpty_ValueIsNewLine")]
		public void IsEmptyCases(string value, bool expectedResult)
		{
			#region Act

			var actualResult = value.IsEmpty();
			
			#endregion

			#region Assert

			Assert.That(actualResult, Is.EqualTo(expectedResult));

			#endregion
		}

		[TestCase("", false, TestName = "HasValue_ValueIsEmptyString")]
		[TestCase("TestValue", true, TestName = "HasValue_ValueIsSampleString")]
		[TestCase(null, false, TestName = "HasValue_ValueIsNull")]
		[TestCase("\n", false, TestName = "HasValue_ValueIsNewLine")]
		public void HasValueCases(string value, bool expectedResult)
		{
			#region Act

			var actualResult = value.HasValue();

			#endregion

			#region Assert

			Assert.That(actualResult, Is.EqualTo(expectedResult));

			#endregion
		}

		[TestCase("", "n1", "n1", TestName = "IsEmptySetValue_ValueIsEmptyString")]
		[TestCase("TestValue", "n2", "TestValue", TestName = "IsEmptySetValue_ValueIsSampleString")]
		[TestCase(null, "n3", "n3", TestName = "IsEmptySetValue_ValueIsNull")]
		[TestCase("\n", "n4", "n4", TestName = "IsEmptySetValue_ValueIsNewLine")]
		public void IsEmptySetValueCases(string value, string newValue, string expectedResult)
		{
			#region Act

			var actualResult = value.IsEmpty(newValue);

			#endregion

			#region Assert

			Assert.That(actualResult, Is.EqualTo(expectedResult));

			#endregion
		}

		[TestCase("", "TestErrorMessage1", true, TestName = "ThrowIfIsEmptySetErrorMessage_ValueIsEmptyString")]
		[TestCase("TestValue", null, false, TestName = "ThrowIfIsEmptySetErrorMessage_ValueIsSampleString")]
		[TestCase(null, "TestErrorMessage2", true, TestName = "ThrowIfIsEmptySetErrorMessage_ValueIsNull")]
		[TestCase("\n", "TestErrorMessage3", true, TestName = "ThrowIfIsEmptySetErrorMessage_ValueIsNewLine")]
		public void ThrowIfIsEmptySetErrorMessageCases(
			string value, string errorMessage, bool exceptionIsExpected)
		{
			#region Arrange

			void ActionForTest()
			{
				value.ThrowIfIsEmpty(errorMessage);
			}

			#endregion

			#region Assert

			if (exceptionIsExpected)
			{
				var actualException = Assert.Throws<InvalidOperationException>(ActionForTest);
				Assert.That(actualException.Message, Is.EqualTo(errorMessage));
			}
			else
				Assert.DoesNotThrow(ActionForTest);

			#endregion
		}

		[TestCase("", "TestErrorMessage1", typeof(Exception), TestName = "ThrowIfIsEmptySetException_ValueIsEmptyStringWaitException")]
		[TestCase("TestValue", null, null, TestName = "ThrowIfIsEmptySetException_WithoutException")]
		[TestCase(null, "TestErrorMessage2", typeof(NullReferenceException), TestName = "ThrowIfIsEmptySetException_WaitNullReferenceException")]
		[TestCase("\n", "TestErrorMessage3", typeof(Exception), TestName = "ThrowIfIsEmptySetException_ValueIsNewLineWaitException")]
		public void ThrowIfIsEmptySetExceptionCases(
			string value, string errorMessage, Type expectedExceptionType)
		{
			#region Arrange

			Exception expectedException = null;

			if (expectedExceptionType != null)
				expectedException = (Exception) Activator.CreateInstance(expectedExceptionType, errorMessage);

			void ActionForTest()
			{
				value.ThrowIfIsEmpty(expectedException);
			}

			#endregion

			#region Assert

			if (expectedExceptionType == null)
				Assert.DoesNotThrow(ActionForTest);
			else
			{
				var actualException = Assert.Throws(Is.TypeOf(expectedExceptionType), ActionForTest);
				Assert.That(actualException.Message, Is.EqualTo(errorMessage));
			}

			#endregion
		}

		[TestCase("", true, TestName = "ThrowIfArgumentIsEmpty_ValueIsEmptyString")]
		[TestCase("TestValue", false, TestName = "ThrowIfArgumentIsEmpty_ValueIsSampleString")]
		[TestCase(null, true, TestName = "ThrowIfArgumentIsEmpty_ValueIsNull")]
		[TestCase("\n", true, TestName = "ThrowIfArgumentIsEmpty_ValueIsNewLine")]
		public void ThrowIfArgumentIsEmptyCases(string value, bool exceptionIsExpected)
		{
			#region Arrange

			const string argumentName = "Argument1";

			void ActionForTest()
			{
				value.ThrowIfArgumentIsEmpty(argumentName);
			}

			#endregion

			#region Assert

			if (exceptionIsExpected)
			{
				var actualException = Assert.Throws<ArgumentNullException>(ActionForTest);

				StringAssert.Contains(argumentName, actualException.Message);
			}
			else
				Assert.DoesNotThrow(ActionForTest);

			#endregion
		}
	}
}