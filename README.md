# Harmony.Infrastructure.Common

It is infrastructure for .NET projects

Contains easy code helpers (like converters and validators).

StringMethods:
- IsEmpty
- HasValue
- ThrowIfIsEmpty

ObjectMethods:
- ThrowIfIsNull

**Examples**

```
// IsEmpty()

var value = ""; // null or spases
var actualResult = value.IsEmpty();
Assert.That(actualResult, Is.True);

// IsEmpty(string)

var value1 = null; // "" or spases
var value2 = "testValue";

var actualResult = value1.IsEmpty(value2);
Assert.That(actualResult, Is.EqualTo(value2));
```
