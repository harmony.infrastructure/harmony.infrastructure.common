﻿using System;

namespace Harmony.Infrastructure.Common.ExtenstionMethods
{
	/// <summary>
	/// Contains extenstion method for object's value
	/// </summary>
	public static class ObjectMethods
	{
		/// <summary>
		/// Implement functional of code "value == null".
		/// </summary>
		/// <param name="value">The value to test</param>
		/// <returns></returns>
		public static bool IsNull<T>(this T value)
		{
			var isNull = value == null;
			return isNull;
		}

		/// <summary>
		///  Invert result of method <see cref="IsNull{T}(T)"/>
		/// </summary>
		/// <param name="value">The string to test</param>
		/// <returns></returns>
		public static bool HasValue<T>(this T value)
		{
			return !value.IsNull();
		}

		/// <summary>
		/// If value is empty then return newValue
		/// </summary>
		/// <param name="value">The value to test.</param>
		/// <param name="newValue">It return if value is empty</param>
		/// <returns>if value is empty then return newValue else return value</returns>
		public static T IsNull<T>(this T value, T newValue)
		{
			return value.IsNull() ? newValue : value;
		}

		/// <summary>
		/// Throw exception if value equals null.
		/// </summary>
		/// <param name="value">The object to test</param>
		/// <param name="exception">The exception to throw.</param>
		public static T ThrowIfIsNull<T>(this T value, Exception exception)
		{
			if (value == null)
				throw exception;
			return value;
		}

		/// <summary>
		/// Throw NullReferenceException if value equals null.
		/// </summary>
		/// <param name="value">The object to test</param>
		/// <param name="errorMessage">The message of NullReferenceException for throw.</param>
		public static T ThrowIfIsNull<T>(this T value, string errorMessage)
		{
			if (value == null)
				throw new NullReferenceException(errorMessage);

			return value;
		}

		/// <summary>
		/// Throw exception (ArgumentException) if argument is equals null
		/// </summary>
		/// <param name="value">The argument to test</param>
		/// <param name="argumentName">Name of argument to test.</param>
		public static T ThrowIfArgumentIsNull<T>(this T value, string argumentName)
		{
			if (value == null)
				throw new ArgumentNullException(argumentName, $"Argument [{argumentName}] is null");
			return value;
		}
	}
}
